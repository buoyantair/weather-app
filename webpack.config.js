const {
  resolve
} = require('path');

module.exports = [{
    entry: resolve(__dirname, 'src/app.scss'),
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      filename: 'style-bundle.js',
      path: resolve(__dirname, 'dist')
    },
    module: {
      rules: [{
        test: /\.scss$/,
        use: [{
            loader: 'file-loader',
            options: {
              name: 'bundle.css',
            },
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              includePaths: ['./node_modules']
            }
          },
        ]
      }]
    },
    devServer: {
      contentBase: resolve(__dirname, 'dist')
    }
  },
  {
    entry: resolve(__dirname, 'src/app.js'),
    output: {
      filename: "bundle.js",
      path: resolve(__dirname, 'dist')
    },
    module: {
      loaders: [{
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      }]
    },
  }
];